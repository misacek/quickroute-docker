# Howto:
#
#  build image
#   git clone https://github.com/loadaverage/electrum.git
#   cd electrum && docker build -t electrum .
#
#  run container
#   docker run --rm \
#        -v ~/.electrum:/home/electrum \
#        -v ~/.themes:/home/electrum/.themes:ro \
#        -v ~/.fonts:/home/electrum/.fonts:ro \
#        -v ~/.icons:/home/electrum/.icons:ro \
#        -v /usr/share/themes:/usr/share/themes:ro \
#        -v /usr/share/fonts:/usr/share/fonts:ro \
#        -v /tmp/.X11-unix:/tmp/.X11-unix \
#        -e DISPLAY=$DISPLAY electrum

FROM debian:stretch

ARG CONTAINER_USER=quickroute
ARG CONTAINER_USER_GID=5005
ARG CONTAINER_USER_ID=5005
ARG DEBIAN_FRONTEND=noninteractive
ARG LABEL_BUILD_DATE
ARG LABEL_URL
ARG LABEL_VCS_REF
ARG LABEL_VCS_TYPE
ARG LABEL_VCS_URL
ARG LABEL_VERSION
ARG PACKAGES="\
tzdata \
quickroute-gps \
"

LABEL \
    org.label-schema.build_date="$LABEL_BUILD_DATE" \
    org.label-schema.name="quickroute-linux" \
    org.label-schema.summary="quickroute-linux in the docker image for linux partability "\
    org.label-schema.url="$LABEL_URL" \
    org.label-schema.vcs-ref="$LABEL_VCS_REF" \
    org.label-schema.vcs-type="$LABEL_VCS_TYPE" \
    org.label-schema.vcs-url="$LABEL_VCS_URL" \
    org.label-schema.version="$LABEL_VERSION" \
    maintainer="Michal Nováček <michal.novacek@gmail.com>"

# ENV QT_X11_NO_MITSHM 1

RUN \
  useradd \
    --uid $CONTAINER_USER_ID \
    --create-home \
    --shell /bin/bash \
    $CONTAINER_USER && \
  apt-get update -qq && \
  apt-get install -y --no-install-recommends $PACKAGES

USER $CONTAINER_USER
CMD ["/usr/bin/quickroute-gps"]
